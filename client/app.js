import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/Home';
import "./stylesheets/main.css";


const Client = ()=>{
     const ChatRoomStyle = {
        width: "100%",
        height: "100%",
      }
      return (
        <div style={ChatRoomStyle}>
          <Home server={"http://localhost:3001/api/"}/>
        </div>
      );
}

ReactDOM.render(<Client />, document.getElementById('container'));
