import React from 'react';

const Inputs = (props = "") => {
    
    /* component inline styles */
    const inputs = {
        marginTop: "4px",
        heght: "30px",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "stretch"
    },
        input = {
            width: "20%",
            borderRadius: "9px",
            border: "1px solid #9d9d9d",
            outline: "none",
            padding: "2%",
            boxSizing: "border-box",
        },
        operators = {
            width: "10%",
            textAlign: "center",
            lineHeight: "30px"
        };

    /* render */
    return (
        <div style={inputs}>
            <input type="number" style={input}></input>
            <span style={operators}>
                *
                </span>
            <input type="number" style={input}></input>
            <span style={operators}>
                =
                </span>
        </div>
    );
};

export default Inputs;