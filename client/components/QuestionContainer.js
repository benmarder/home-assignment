import React from 'react';
import axios from 'axios';
import AnswerContainer from './answerContainer';

export default class QuestionContainer extends React.Component {
    constructor(props) {
        super(props);
        let method = "",
            apiQuery = "";
        /*Categorize the state acording to Question */
        switch (props.name) {
            case "Get server time":
                method = "get";
                apiQuery = "time"
                break;
            case "Get server calls":
                method = "get"
                apiQuery = "s_calls"
                break;
            case "Multiplay numbers":
                method = "post"
                apiQuery = "mult"
                break;
        }
        this.state = {
            url: props.server + apiQuery,
            method: method,
            visibility: "hidden"  //answer is hidden at first
        };
        /*bind state related functions*/
        this.handleClick = this.handleClick.bind(this);
    }//end of constructor()
    render() {
        console.log("-QuestionContainer- rendering! url check:" + this.state.url);
        return (
            <div style={styles.component}>
                <button
                    style={styles.button}
                    onClick={this.handleClick}
                    className="animated">
                    {"Click for task #" + this.props.num}
                </button>
                <section style={styles.container}>
                    <AnswerContainer
                        name={this.props.name}
                        url={this.state.url}
                        method={this.state.method}
                        style={{ visibility: this.state.visibility }} />
                </section>
            </div>
        );
    }
    /* show/hide the AnswerContainer component */
    handleClick(event) {
        let toggle = this.state.visibility;
        if (toggle === "hidden")
            toggle = "visible";
        else
            toggle = "hidden";
        event.target.classList.toggle("active");
        this.setState({
            ...this.state,
            visibility: toggle
        });
    }
}//end of class QuestionContainer

/* component inline styles */
const styles = {
    component: {
        height: "300px",
        width: "25%",
        display: "flex",
        flexDirection: "column",
    },
    button: {
        outline: "none",
        fontSize: "1.3em",
        fontWeight: "bold",
        color: "#FFFFF0",
        background: "none",
        textAlign: "center",
        height: "40px",
        marginBottom: "10px",
        border: "2px solid #FFFFF0"
    },
    container: {
        height: "250px",
        border: "2px solid #FFFFF0"
    }
}