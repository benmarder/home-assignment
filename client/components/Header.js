import React from 'react';
const Header = (props = "") => {
  
    /* component inline styles */
    const headerStyles = {
        marginTop: "20px",
        display: "flex",
        flexDirection: "row",
        color: "#FFFFF0"
    },
    titleStyles = {
        fontSize: "2.5em",
        margin: "0 4px",
        lineHeight: "0.8",
    },
    authorStyles = {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-end",
        fontSize: "1.1em",
        fontWeight: "normal",
        margin: "0 2px",
        lineHeight: "0.8",
    },
    spaceStyles = {
        borderBottom: "2px solid #FFFFF0",
        width: "6%"
    },
    spaceStylesFlex = {
        borderBottom: "2px solid #FFFFF0",
        flex: "auto"
    };
    
    /* render */
    return (
        <header style={headerStyles}>
            <div style={spaceStyles}></div>
            <h1 style={titleStyles}>
                {props.title.split('\\n').map((item, key, arr) => {
                    return <span key={key}>{item}<br /></span>
                })}
            </h1>
            <div style={spaceStylesFlex}></div>
            <h3 style={authorStyles}>{props.author}</h3>
            <div style={spaceStyles}></div>
        </header>
    );
};

export default Header;