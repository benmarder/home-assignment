import React from 'react';
import axios from 'axios';
import Inputs from './inputs';


export default class AnswerContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      result: null,
      url: props.url,
      method: props.method,
      name: props.name
    };
    /*bind state related functions*/
    this.updateResult = this.updateResult.bind(this);
    this.handleClick = this.handleClick.bind(this);

  }

  render() {
    console.log("-AnswerContainer- rendering! url check:" + this.state);
    let inputs = "";
    if (this.state.method === "post") {//multiply quwstion needs inputs
      inputs = <Inputs />
    }

    return (
      <div style={{ ...this.props.style, ...styles.component }}>
        <button style={styles.button} onClick={this.handleClick}>{this.state.name}!</button>
        <section style={styles.answer}>
          {inputs}
          <div style={styles.centerAnswer}>
            {this.state.result}
          </div>
        </section>
      </div>
    );
  }
/*function that updates the state acording to server response  */
  updateResult(response) {
    const answer = (response.data);
    if (answer && answer.confirmation === "success") {
      const res = answer.result;
      console.log("success!", res)
      this.setState({
        result: answer.type !== "Date" ? res : Date(res)
      });
    }
    else if(answer && answer.confirmation === "fail")
      alert(answer.message)
    }

/*function that controlls which request will be sended to the server */
  handleClick() {
    const method = this.state.method,
          url = this.state.url;
    console.log(`-AnswerContainer- url check:${url} method check:${method}`);
    if (method === "post") { // its the Multiplay question
      /* get all numbers to multiply */
      const inputs = document.querySelectorAll("input");
      let numbersArray = [];
      for (let input of inputs) {
        numbersArray.push(input.value)
      }
      console.log("-AnswerContainer- numbers to multiply:", numbersArray);

      axios.post(url, { numbers: numbersArray })
        .then(this.updateResult)
        .catch((error) => {
          alert("oops! internal server error")
        });
    }
    else if (method === "get") {// its the time and counter questions
      axios.get(url)
        .then(this.updateResult)
        .catch((error) => {
          alert("oops! internal server error")
        });
    }
  }//end of handleClick()

}//end of class AnswerContainer

const styles = {
  component: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#002263",
  },
  button: {
    height: "40%",
    backgroundColor: "#34495e",
    color: "#FFFFF0",
    fontWeight: "bold",
    fontSize: "1.3em"
  },
  answer: {
    height: "60%",
    display: "flex",
    flexDirection: "column",
    color: "#FFFFF0",
    fontWeight: "bold"
  },
  centerAnswer: {
    flex: "auto",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    textAlign: "center",
    fontSize: "1.5em"
  }

}//end of class styles