import React from 'react';
import Header from './Header';
import QuestionContainer from "./QuestionContainer";

const Home = (props)=> {
  /* component inline styles */
  const styles = {
    component: {
      display: "flex",
      flexDirection: "column"
    },
    answers:{
      marginTop:"80px",
     display:"flex",
     flexDirection: "row",
     justifyContent:"space-evenly"
    }
  }
    /* render */
    return (
      <div style={styles.component}>
        <Header title="Home\nAssignment" breakTitle={true} author="Ben Marder"/>
        <main style={styles.answers}>
          <QuestionContainer name="Get server time" num={1} server={props.server} />
          <QuestionContainer name="Get server calls" num={2} server={props.server}/>
          <QuestionContainer name="Multiplay numbers" num={3} server={props.server} />
        </main>
      </div>
    );
  }
  export default Home;



