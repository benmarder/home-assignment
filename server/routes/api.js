var express     = require("express"),
    router      = express.Router(),
    serverCalls = require("../modules/serverCalls");


router.get("/time", function (req, res) {
    res.json({
        confirmation: "success",
        result: new Date().getTime(),
        type:"Date"
    });
});

router.get("/s_calls", function (req, res) {
    const counter = serverCalls.get();
    if(counter){
        res.json({
            confirmation: "success",
            result: serverCalls.get()
        });
    }
    else{
        res.json({
            confirmation: "fail",
            message: "internal error : number too big"
        });
    }
   
});
router.post("/mult", function (req, res) {
    console.log(" got :",req.body);
    const numArray = req.body.numbers;
    if(numArray instanceof Array){
        const result = numArray.reduce((prev, curr) => prev * curr, 1);
        res.json({
            confirmation: "success",
            result: result
        });
    }
    else {
        res.json({
            confirmation: "fail",
            message: "expected array but got something else"
        });
    }
});
router.get("*", function (req, res) {
    res.json({
        confirmation: "fail",
        message: "you are in the api but you are lost... "
    });
});
module.exports = router;