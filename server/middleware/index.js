// app middlewares
var  serverCalls  = require("../modules/serverCalls");

var middleware = {}

middleware.incOnEveryRequest = (req,res,next)=>{
    serverCalls.inc(); 
    next();
}

middleware.setCors =(req, res, next)=>{
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers','*');
    next()
};
module.exports = middleware;