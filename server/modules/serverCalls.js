/**/
var counter = 0,
tooBig=false;
ServerCalls = {}
ServerCalls.inc = ()=>{
    if(++counter === Number.MAX_VALUE)
        tooBig=true
};
ServerCalls.get = ()=>{
    if (tooBig)
        return false;
    return counter;
}
    

module.exports = ServerCalls;