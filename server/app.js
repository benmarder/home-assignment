var express   = require("express"),
 app          = express(),
 bodyParser   = require("body-parser"),
 middleware   = require("./middleware"),
//requiring routs
 apiRouts     = require("./routes/api");
 

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.set('view engine', 'ejs'); 
app.use(express.static(__dirname+"/public"));// Add support for static files

//middlewares
app.use(middleware.incOnEveryRequest);
app.use(middleware.setCors);
//ROUTS
app.use("/api",apiRouts);

app.get('*', function(req, res){
    res.render('notFound');
  });
app.listen(3001, 'localhost', () => {
    console.log("server running on %s mode port 3001", app.settings.env);
  });